﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ScrollApp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer.
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private string  _numberOneString = "";
        private string  _numberTwoString = "";
        private string _printString = "";
        private int      symbol = 0;
        private decimal _numberOne;
        private decimal _numberTwo;

        public MainPage()
        {
            InitializeComponent();
        }
        // I used this to understand basic of workingthe help of https://github.com/xamarin/mobile-samples/blob/master/LivePlayer/BasicCalculator/Calculator/

        private void reinitialize()
        {
            _printString = "";
            _numberOneString = "";
            _numberTwoString = "";
            _numberOne = 0;
            _numberTwo = 0;
            Calcul.Text = _printString;
            symbol = 0;
        }

        private void BtnOperator_Clicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string pressed = button.Text;
            Result.Text = "";

            if (pressed == "+")
            {
                _printString = $"{_numberOneString}+{_numberTwoString}";
                Calcul.Text = _printString;
                symbol = 1;
            }
            else if (pressed == "-")
            {
                _printString = $"{_numberOneString}-{_numberTwoString}";
                Calcul.Text = _printString;
                symbol = 2;
            }
            else if (pressed == "C")
            {
                reinitialize();
                Result.Text = "";
            }
            else
            {
                Result.Text = $"{(symbol == 1 ? (_numberOne + _numberTwo) : (_numberOne - _numberTwo))}";
                reinitialize();
            }
        }
        private void BtnNumber(object sender, EventArgs e)
        {
            Result.Text = "";
            decimal parsed;
            Button button = (Button)sender;
            string pressed = button.Text;
            if (symbol == 0)
            {
                _numberOneString = $"{_numberOneString}{pressed}";
                Calcul.Text = _numberOneString;
                Decimal.TryParse(_numberOneString, out _numberOne);
            }
            else
            {
                _numberTwoString = $"{_numberTwoString}{pressed}";
                Calcul.Text = $"{_numberOneString}{(symbol == 0 ? "" : (symbol == 1 ? "+" : "-") )}{_numberTwoString}";
                Decimal.TryParse(_numberTwoString, out _numberTwo);
            }
        }
    }
}
